/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.web;

import com.kaluzny.interfaceRest.ProductQuantity;
import com.kaluzny.interfaceRest.ShoplistProduct;
import com.kaluzny.persistence.Product;
import com.kaluzny.persistence.ShopListDetails;
import com.kaluzny.service.ProductSevice;
import com.kaluzny.service.ShoplistProductService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class ProductRestController {

    @Autowired
    private ProductSevice productService;
    
    @Autowired
    private ShoplistProductService shoplistProductService;

    @RequestMapping(value = "/getAllproducts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getAllproducts() {
        try {
            return productService.getAllproducts();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @RequestMapping(value = "/getAllProductById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductQuantity> getAllProductById(@PathVariable Long id) {
        try {
            return productService.getAllProductById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @RequestMapping(value = "/postProduct", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Product postProduct(@RequestBody Product product) {
        try {
            return productService.postProduct(product);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    @RequestMapping(value = "/postOrderProduct", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ShopListDetails postOrderProduct(@RequestBody ShoplistProduct shoplistProduct) {
        try {
            return shoplistProductService.postOrderProduct(shoplistProduct);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    @RequestMapping(value = "/deleteProduct/{id}", method = RequestMethod.DELETE)
    public void deleteProduct(@PathVariable Long id) {
        try {
            productService.deleteProduct(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
   
}
