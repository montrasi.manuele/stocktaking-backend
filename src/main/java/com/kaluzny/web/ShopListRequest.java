/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.web;

import com.kaluzny.persistence.ShopList;
import com.kaluzny.persistence.ShopListDetails;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author LNV_81MVS2IX
 */
public class ShopListRequest {
    private Optional<ShopList> list;
    private List<ShopListDetails> productIds;

    /**
     * @return the list
     */
    public Optional<ShopList> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(Optional<ShopList> list) {
        this.list = list;
    }

    /**
     * @return the productIds
     */
    public List<ShopListDetails> getProductIds() {
        return productIds;
    }

    /**
     * @param productIds the productIds to set
     */
    public void setProductIds(List<ShopListDetails> productIds) {
        this.productIds = productIds;
    }
}
