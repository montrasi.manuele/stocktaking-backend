package com.kaluzny.web;

import com.kaluzny.interfaceRest.Filter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.kaluzny.persistence.ShopList;
import com.kaluzny.persistence.ShopListDetails;
import com.kaluzny.domain.ShopListDetailsRepository;
import com.kaluzny.domain.ShopListRepository;
import com.kaluzny.interfaceRest.ShoplistProduct;
import com.kaluzny.service.OrderProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class ShopListRestController {

    @Autowired
    private OrderProductService service;

    private final ShopListRepository repository;
    private final ShopListDetailsRepository detailsRepository;

   
    
    @RequestMapping(value = "/getAllOrderProduct", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ShoplistProduct> getAllOrderProduct() {
        try {
            return service.getAllOrderProduct();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @RequestMapping(value = "/getAllOrder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ShopList> getAllOrder() {
        try {
            return service.getAllOrder();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    
    @RequestMapping(value = "/getOrderByListname/{listname}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ShopList getOrderByListname(@PathVariable String listname) {
        try {
            if (!listname.isEmpty() && listname != null) {
                return service.getOrderByListname(listname.trim());
            } else {
                return null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    
    
    
    @RequestMapping(value = "/shoplists", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<ShopList> getShopLists(@RequestBody Filter filter) {
        if (filter.getTerm() != null && filter.getTerm() != "") {
            return (List<ShopList>) repository.findPaginatedWithFilter(filter.getTerm().toLowerCase(), filter.getIndex() * filter.getSize(), filter.getSize());
        } else {
            return (List<ShopList>) repository.findPaginated(filter.getIndex() * filter.getSize(), filter.getSize());
        }
    }

    @RequestMapping(value = "/shoplist/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ShopListRequest getShopList(@PathVariable Long id) {
        ShopListRequest response = new ShopListRequest();
        Optional<ShopList> list = repository.findById(id);
        List<ShopListDetails> products = detailsRepository.findByOrderId(id);
        response.setList(list);
        response.setProductIds(products);
        return response;
    }

    @RequestMapping(value = "/shoplist/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean removeShopListById(@PathVariable Long id) {
        repository.deleteById(id);
        return true;
    }

    @RequestMapping(value = "/shoplist", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public boolean setShopList(@RequestBody ShopListRequest request) {
        Optional<ShopList> list = request.getList();
        // elimina prodotti precedenti se update
        list.ifPresent(l -> {
            try {
                detailsRepository.deleteByOrderId(l.getId());
            } catch (Exception e) {

            }
            // salva l'ordine
            repository.save(l);
        });
        List<ShopListDetails> products = request.getProductIds();
        for (ShopListDetails product : products) {
            detailsRepository.save(product);
        }
        return true;
    }

    @RequestMapping(value = "/test/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<ShopListDetails> test(@PathVariable Long id) {
        return detailsRepository.findAll();
        //return detailsRepository.findByOrderId(id);

    }
}
