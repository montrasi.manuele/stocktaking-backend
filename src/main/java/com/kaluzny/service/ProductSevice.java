/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.service;

import com.kaluzny.persistence.Product;
import com.kaluzny.domain.ProductRepository;
import com.kaluzny.domain.ShopListDetailsRepository;
import com.kaluzny.interfaceRest.ProductQuantity;
import com.kaluzny.persistence.ShopListDetails;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author montr
 */
@Component
public class ProductSevice {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ShopListDetailsRepository shopListDetailsRepository;

    private String ERROR_PRODUCT_EXIST = "Product already existing in the product list";

    public List<Product> getAllproducts() {
        try {
            return ((List<Product>) repository.findAll());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<ProductQuantity> getAllProductById(Long id) {
        try {
            List<ShopListDetails> listShopListDetails = shopListDetailsRepository.getAllQuantityProductsById(id);
            //List<Product> listProduct = repository.getAllProductsById(id);
            List<ProductQuantity> listProductQuantity = new ArrayList<>();

            if (!listShopListDetails.isEmpty() && listShopListDetails != null) {
                for (ShopListDetails shopListDetails : listShopListDetails) {
                    Long idProduct = shopListDetails.getId_product();
                    Product product = repository.getProductsById(idProduct);
                    if (product != null) {
                        ProductQuantity productQuantity = new ProductQuantity();

                        productQuantity.setId(product.getId());
                        productQuantity.setName(product.getName());
                        productQuantity.setDescription(product.getDescription());
                        productQuantity.setPrice(product.getPrice());
                        productQuantity.setQuantity(shopListDetails.getQuantity());

                        listProductQuantity.add(productQuantity);
                    }
                }
                return listProductQuantity;
            } else {
                return null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Product postProduct(Product product) {
        boolean productExitst = false;
        try {
            List<String> listStringName = repository.getAllNameProduct();

            if (!listStringName.isEmpty()) {
                for (String stringName : listStringName) {
                    if (product.getName().equals(stringName)) {
                        productExitst = true;
                        break;
                    }
                }
            }
            if (!productExitst) {
                return repository.save(product);
            } else {
                return null;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void deleteProduct(Long id) {
        try {
            repository.deleteById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
