/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.service;

import com.kaluzny.domain.ShopListRepository;
import com.kaluzny.interfaceRest.ShoplistProduct;
import com.kaluzny.persistence.Product;
import com.kaluzny.persistence.ShopList;
import com.kaluzny.persistence.ShopListDetails;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author montr
 */
@Component
public class OrderProductService {

    @Autowired
    private ShopListRepository shopListRepository;

    public List<ShoplistProduct> getAllOrderProduct() {
        return null; //shopListRepository.getAllOrderProduct();
    }

    public ShopList getOrderByListname(String listname) {
        listname = "%" + listname + "%";
        return shopListRepository.getOrderByListname(listname);
    }

    public List<ShopList> getAllOrder() {
        return (List<ShopList>) shopListRepository.findAll();
    }

    public ShopList saveOrder(ShopList shopList) {
        return shopListRepository.save(shopList);
    }
    
    public ShopList checkOrderByName(String listname) {
        return shopListRepository.checkOrderByName(listname.toLowerCase());
    }

    public void updatePriceStore(Long id, double price_store) {
       shopListRepository.updatePriceStore(id, price_store);
    }

}
