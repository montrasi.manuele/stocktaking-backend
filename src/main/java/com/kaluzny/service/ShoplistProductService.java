/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.service;

import com.kaluzny.domain.ShopListDetailsRepository;
import com.kaluzny.interfaceRest.ShoplistProduct;
import com.kaluzny.persistence.Product;
import com.kaluzny.persistence.ShopList;
import com.kaluzny.persistence.ShopListDetails;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author montr
 */
@Component
public class ShoplistProductService {

    @Autowired
    private ShopListDetailsRepository shopListDetailsRepository;

    @Autowired
    private OrderProductService orderProductService;

    public ShopListDetails postOrderProduct(ShoplistProduct shoplistProduct) {
        if (shoplistProduct != null) {
            ShopList shopList = new ShopList();
            ShopListDetails shopListDetails = new ShopListDetails();
            try {
                ShopList checkNameExist = orderProductService.checkOrderByName(shoplistProduct.getListname());
                ShopList idShopList = null;

                if (checkNameExist == null) {
                    double priceFirstOrder = (shoplistProduct.getPrice() * shoplistProduct.getQuantity());
                            
                    shopList.setListname(shoplistProduct.getListname());
                    shopList.setDate_order(new Date());
                    shopList.setExpiration_date(shoplistProduct.getExpiration_order() != null ? shoplistProduct.getExpiration_order() : null);
                    shopList.setPrice_store(priceFirstOrder);
                    shopList.setCompleted(shoplistProduct.isCompleted());

                    idShopList = orderProductService.saveOrder(shopList);
                    shopListDetails.setId_order(idShopList.getId());
                } else {
                    double updatePriceStore = (shoplistProduct.getPrice() * shoplistProduct.getQuantity()) + checkNameExist.getPrice_store();
                    orderProductService.updatePriceStore(checkNameExist.getId(), updatePriceStore);
                    shopListDetails.setId_order(checkNameExist.getId());
                }

                shopListDetails.setId_product(shoplistProduct.getId_product());
                shopListDetails.setQuantity(shoplistProduct.getQuantity());

                if (shopListDetails != null) {
                    return shopListDetailsRepository.save(shopListDetails);
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return null;
    }
}
