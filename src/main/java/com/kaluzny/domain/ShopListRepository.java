package com.kaluzny.domain;

import com.kaluzny.persistence.ShopList;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ShopListRepository  extends CrudRepository<ShopList, Long> {
    @Query(value = "SELECT * FROM order_client OFFSET ?1 LIMIT ?2", nativeQuery = true)
    public List<ShopList> findPaginated(int index, int size);
    
    @Query(value = "SELECT * FROM order_client WHERE LOWER(username) LIKE %?1% OFFSET ?2 LIMIT ?3", nativeQuery = true)
    public List<ShopList> findPaginatedWithFilter(String filter, Integer index, Integer size);
    
    @Query(value = "SELECT * FROM order_client WHERE LOWER(listname) LIKE :listname", nativeQuery = true)
    public ShopList getOrderByListname(@Param("listname") String listname);

    @Query(value = "SELECT * FROM order_client WHERE LOWER(listname) = :listname", nativeQuery = true)
    public ShopList checkOrderByName(@Param("listname") String listname);

    @Modifying
    @Transactional
    @Query(value = "UPDATE order_client SET price_store = :price_store WHERE id = :id", nativeQuery = true)
    public void updatePriceStore(@Param("id") Long id, @Param("price_store") double price_store); 
    
}
