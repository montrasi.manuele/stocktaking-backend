/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaluzny.domain;

import com.kaluzny.persistence.ShopListDetails;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ShopListDetailsRepository  extends CrudRepository<ShopListDetails, Long>  {
    
    @Query(value = "SELECT * FROM order_product WHERE id_order = :id", nativeQuery = true)
    public List<ShopListDetails> findByOrderId(Long id);
    
    @Query(value = "DELETE FROM order_product WHERE id_order = :id", nativeQuery = true)
    public List<ShopListDetails> deleteByOrderId(Long id);    
    
    @Query(value = "SELECT order_product.id, order_product.id_order, order_product.id_product, order_product.quantity FROM order_product WHERE order_product.id_order = :id", nativeQuery = true)
    public List<ShopListDetails> getAllQuantityProductsById(@Param("id") Long id);
}
