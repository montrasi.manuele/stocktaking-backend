package com.kaluzny.domain;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.kaluzny.persistence.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query(value = "SELECT * FROM product OFFSET ?1 LIMIT ?2", nativeQuery = true)
    public List<Product> findPaginated(int index, int size);

    @Query(value = "SELECT * FROM product WHERE LOWER(name) LIKE %?1% OFFSET ?2 LIMIT ?3", nativeQuery = true)
    public List<Product> findPaginatedWithFilter(String filter, Integer index, Integer size);

    @Query(value = "SELECT product.name FROM product", nativeQuery = true)
    public List<String> getAllNameProduct();
    
    @Query(value = "SELECT product.id, product.name, product.description, product.price, order_product.quantity FROM product, order_product WHERE order_product.id_order = :id", nativeQuery = true)
    public List<Object[]> getAllProductById(@Param("id") Long id);

    @Query(value = "SELECT product.id, product.name, product.description, product.price FROM product INNER JOIN order_product ON order_product.id_order = :id GROUP BY product.id", nativeQuery = true)
    public List<Product> getAllProductsById(@Param("id") Long id);

    @Query(value = "SELECT product.id, product.name, product.description, product.price FROM product WHERE product.id = :id ", nativeQuery = true)
    public Product getProductsById(@Param("id") Long id);
    
    
}
